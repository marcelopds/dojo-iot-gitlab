#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include "alturas.h"

//Configuracoes da rede wifi utilizada para conectar a internet
const char* nomeRedeWifi = "Marcelao";
const char* senha = "********";

/**Token gerado no site do gitlab (Personal Access Tokens). Settings > Access Tokens */
const char* tokenGitlab = "edBaeQ-P2SNitxy-vUZ9";
int id = 0;
#define D2   12

int ledPin = 13;

//led for visualization (use 13 for built-in led) 
int speakerPin = 4;




//Função: inicializa o output em nível lógico baixo
//Parâmetros: nenhum
//Retorno: nenhum
void InitOutput()
{
    //IMPORTANTE: o Led já contido na placa é acionado com lógica invertida (ou seja,
    //enviar HIGH para o output faz o Led apagar / enviar LOW faz o Led acender)
     pinMode(D2, OUTPUT);
     digitalWrite(D2,  HIGH);    

     pinMode(ledPin, OUTPUT);
     digitalWrite(ledPin,  LOW);    
     
     pinMode(speakerPin, OUTPUT);
     digitalWrite(speakerPin,  HIGH);    
}

//metodo com as configuracoes inicias necessarias para o programa iniciar
void setup() {
  InitOutput();
  Serial.begin(115200);
  WiFi.begin(nomeRedeWifi,senha);
  
  //Aguardando a conexao com a rede wifi
  while (WiFi.status() != WL_CONNECTED) { 
    delay(1000);
    Serial.println("Conectando com a rede WIFI ..."); 
  }
}

void tocarBuzzer(int note, int tempo){
    
  int frequency = note; //Specified in Hz
  int buzzPin = D2; 
  int timeOn = tempo; //specified in milliseconds
  int timeOff = tempo; //specified in millisecods
  
  tone(buzzPin, frequency);
  delay(timeOn);
  noTone(buzzPin);
  
}

//codigo que sera repetido varias vezes
void loop() {
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
      Serial.println("Conectado iniciando requisicao!");     
      HTTPClient http;  //Declare an object of class HTTPClient

    
      //site para encontar o codigo do certificado de seguranca https://www.grc.com/fingerprints.htm 5C:20:91:BF:0F:B8:E6:D7:D4:08:42:94:AA:23:86:1A:3A:8A:E9:83
      http.begin("https://gitlab.com/api/v4/merge_requests?private_token=rBtNZywT8VQzV27QZHb4&state=opened&view=simple&project_id=7773181&target_branch=master", "5C 20 91 BF 0F B8 E6 D7 D4 08 42 94 AA 23 86 1A 3A 8A E9 83");  //Specify request destination
    
      //Token criado no gitalab
      http.setAuthorization(tokenGitlab);
      int httpCode = http.GET();                                                                  //Send the request
      Serial.println("httpCode: ");  
      Serial.print(httpCode);
      
      //Codigo http que retorna da requisicao  
      if ( httpCode > 0) {  
        String JSONMessage = http.getString();   // JSON retornado
      
        if (JSONMessage == "[]") {
           id = 0;
           Serial.print("Sem MR aberto!");
        } else if ( id == 0 ){
           id = 1;
           Serial.print("MR aberto!");
           march();
        }       
      }
 
      http.end();   //Close connection 
  }
 
  delay(30000);    //Send a request every 30 seconds
}

void beep ( int frequencyInHertz, long timeInMilliseconds)
{ 
    digitalWrite(ledPin, HIGH);   
    //use led to visualize the notes being played
    
    int x;   
    long delayAmount = (long)(1000000/frequencyInHertz);
    long loopTime = (long)((timeInMilliseconds*1000)/(delayAmount*2));
    for (x=0;x<loopTime;x++)   
      {    
          digitalWrite(speakerPin,HIGH);
          delayMicroseconds(delayAmount);
          digitalWrite(speakerPin,LOW);
          delayMicroseconds(delayAmount);
      }    
    
    digitalWrite(ledPin, LOW);
    //set led back to low
    
    delay(20);
    //a little delay to make all notes sound separate
}    
 
void march()
{    
    //for the sheet music see:
    //http://www.musicnotes.com/sheetmusic/mtd.asp?ppn=MN0016254
    //this is just a translation of said sheet music to frequencies / time in ms
    //used 500 ms for a quart note
    
    beep( a, 500); 
    beep( a, 500);     
    beep( a, 500); 
    beep( f, 350); 
    beep( cH, 150);
    
    beep( a, 500);
    beep( f, 350);
    beep( cH, 150);
    beep( a, 1000);
    //first bit
    
    beep( eH, 500);
    beep( eH, 500);
    beep( eH, 500);    
    beep( fH, 350); 
    beep( cH, 150);
    
    beep( gS, 500);
    beep( f, 350);
    beep( cH, 150);
    beep( a, 1000);
    //second bit...
    
    beep( aH, 500);
    beep( a, 350); 
    beep( a, 150);
    beep( aH, 500);
    beep( gSH, 250); 
    beep( gH, 250);
    
    beep( fSH, 125);
    beep( fH, 125);    
    beep( fSH, 250);
    delay(250);
    beep( aS, 250);    
    beep( dSH, 500);  
    beep( dH, 250);  
    beep( cSH, 250);  
    //start of the interesting bit
    
    beep( cH, 125);  
    beep( b, 125);  
    beep( cH, 250);      
    delay(250);
    beep( f, 125);  
    beep( gS, 500);  
    beep( f, 375);  
    beep( a, 125); 
    
    beep( cH, 500); 
    beep( a, 375);  
    beep( cH, 125); 
    beep( eH, 1000); 
    //more interesting stuff (this doesn't quite get it right somehow)
    
    beep( aH, 500);
    beep( a, 350); 
    beep( a, 150);
    beep( aH, 500);
    beep( gSH, 250); 
    beep( gH, 250);
    
    beep( fSH, 125);
    beep( fH, 125);    
    beep( fSH, 250);
    delay(250);
    beep( aS, 250);    
    beep( dSH, 500);  
    beep( dH, 250);  
    beep( cSH, 250);  
    //repeat... repeat
    
    beep( cH, 125);  
    beep( b, 125);  
    beep( cH, 250);      
    delay(250);
    beep( f, 250);  
    beep( gS, 500);  
    beep( f, 375);  
    beep( cH, 125); 
           
    beep( a, 500);            
    beep( f, 375);            
    beep( c, 125);            
    beep( a, 1000);       
    //and we're done \ó/    
}
